package py.com.prutec.stock.ventas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.prutec.stock.ventas.model.Cliente;
import py.com.prutec.stock.ventas.repository.ClienteRepository;
import py.com.prutec.stock.ventas.service.ClienteService;

/**
 *
 * @author pbbattilana
 */
@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public Cliente crearCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }
}

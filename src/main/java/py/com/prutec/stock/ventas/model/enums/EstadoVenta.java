package py.com.prutec.stock.ventas.model.enums;

/**
 *
 * @author pbbattilana
 */
public enum EstadoVenta {
    PROCESADO("p"),
    PENDIENTE("np"),
    PROCESADO_CON_ERROR("pe");

    private final String codigo;

    EstadoVenta(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public static EstadoVenta fromCodigo(String codigo) {
        for (EstadoVenta estado : EstadoVenta.values()) {
            if (estado.getCodigo().equalsIgnoreCase(codigo)) {
                return estado;
            }
        }
        throw new IllegalArgumentException("Código de estado no válido: " + codigo);
    }
}

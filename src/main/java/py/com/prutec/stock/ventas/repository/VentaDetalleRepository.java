package py.com.prutec.stock.ventas.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import py.com.prutec.stock.ventas.model.VentaDetalle;

/**
 *
 * @author pbbattilana
 */
@Repository
public interface VentaDetalleRepository extends JpaRepository<VentaDetalle, Long> {

    List<VentaDetalle> findByProductoIdAndEstado(Long productoId, String estado);

    VentaDetalle findByVentaCabeceraId(Long ventaCabeceraId);

}

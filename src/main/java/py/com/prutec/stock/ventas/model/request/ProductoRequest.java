package py.com.prutec.stock.ventas.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author pbbattilana
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductoRequest {
    private Long idProducto;
    private int cantidad;
}

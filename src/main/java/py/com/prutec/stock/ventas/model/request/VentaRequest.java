package py.com.prutec.stock.ventas.model.request;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VentaRequest {
    private Long clienteId;
    private List<ProductoRequest> productos;
}

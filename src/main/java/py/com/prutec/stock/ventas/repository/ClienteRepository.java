package py.com.prutec.stock.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import py.com.prutec.stock.ventas.model.Cliente;

/**
 *
 * @author pbbattilana
 */
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    
}
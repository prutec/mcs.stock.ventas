package py.com.prutec.stock.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import py.com.prutec.stock.ventas.model.Producto;

/**
 *
 * @author pbbattilana
 */
@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {
    
}
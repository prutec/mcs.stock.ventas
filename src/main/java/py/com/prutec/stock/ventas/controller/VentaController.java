package py.com.prutec.stock.ventas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import py.com.prutec.stock.ventas.messaging.RabbitMQSender;
import py.com.prutec.stock.ventas.model.Cliente;
import py.com.prutec.stock.ventas.model.VentaCabecera;
import py.com.prutec.stock.ventas.model.VentaDetalle;
import py.com.prutec.stock.ventas.model.request.ProductoRequest;
import py.com.prutec.stock.ventas.model.request.VentaRequest;
import py.com.prutec.stock.ventas.service.impl.VentaServiceImpl;

/**
 *
 * @author pbbattilana
 */
@RestController
@RequestMapping("/venta")
public class VentaController {

    @Autowired
    private VentaServiceImpl ventaService;

    @PostMapping
    public ResponseEntity<String> realizarVenta(@RequestBody VentaRequest data
    ) {
        
        boolean stockDisponible = ventaService.verificarStockDisponible(data.getProductos());
        if (!stockDisponible) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Stock insuficiente para la compra");
        }
        ventaService.realizarVenta(data);
        return ResponseEntity.status(HttpStatus.OK).body("Compra realizada con éxito (En proceso de actualización)");
    }
}

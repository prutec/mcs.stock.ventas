package py.com.prutec.stock.ventas.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.prutec.stock.ventas.model.VentaDetalle;

/**
 *
 * @author pbbattilana
 */
@Slf4j
@Component
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    private static final String EXCHANGE_NAME = "exchange.stock";
    private static final String ROUTING_KEY = "ventas.stock.update";

    public void enviarMensajeVenta(VentaDetalle detalle) {
        String mensaje = detalle.getId() + ":" + detalle.getProducto().getId() + ":" + detalle.getCantidad();
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, ROUTING_KEY, mensaje);
        log.info("Mensaje enviado a RabbitMQ para actualizar stock: {}", mensaje);
    }
}


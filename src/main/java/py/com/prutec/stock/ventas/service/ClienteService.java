package py.com.prutec.stock.ventas.service;

import py.com.prutec.stock.ventas.model.Cliente;

/**
 *
 * @author pbbattilana
 */
public interface ClienteService {

    public Cliente crearCliente(Cliente cliente);
}

package py.com.prutec.stock.ventas.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import py.com.prutec.stock.ventas.messaging.RabbitMQSender;
import py.com.prutec.stock.ventas.model.Cliente;
import py.com.prutec.stock.ventas.model.Producto;
import py.com.prutec.stock.ventas.model.VentaCabecera;
import py.com.prutec.stock.ventas.model.VentaDetalle;
import py.com.prutec.stock.ventas.model.enums.EstadoVenta;
import py.com.prutec.stock.ventas.model.request.ProductoRequest;
import py.com.prutec.stock.ventas.model.request.VentaRequest;
import py.com.prutec.stock.ventas.repository.ClienteRepository;
import py.com.prutec.stock.ventas.repository.ProductoRepository;
import py.com.prutec.stock.ventas.repository.VentaCabeceraRepository;
import py.com.prutec.stock.ventas.repository.VentaDetalleRepository;
import py.com.prutec.stock.ventas.service.VentaService;

/**
 *
 * @author pbbattilana
 */
@Slf4j
@Service
public class VentaServiceImpl implements VentaService {

    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private VentaCabeceraRepository ventaCabeceraRepository;

    @Autowired
    private VentaDetalleRepository ventaDetalleRepository;

    @Autowired
    private RabbitMQSender rabbitMQSender;

    @Transactional
    @Override
    public void realizarVenta(VentaRequest ventas) {
        Optional<Cliente> cliente = clienteRepository.findById(ventas.getClienteId());
        VentaCabecera cabecera = new VentaCabecera();
        cabecera.setCliente(cliente.get());
        cabecera.setFecha(LocalDate.now());
        ventaCabeceraRepository.save(cabecera);
        log.info("VentaCabecera creada con ID: {}", cabecera.getId());
        Producto producto;
        VentaDetalle detalle;
        for (ProductoRequest productoRequest : ventas.getProductos()) {
            producto = productoRepository.findById(productoRequest.getIdProducto()).orElseThrow(() -> new IllegalArgumentException("Producto no encontrado"));
            detalle = new VentaDetalle();
            detalle.setVentaCabecera(cabecera);
            detalle.setProducto(producto);
            detalle.setCantidad(productoRequest.getCantidad());
            detalle.setEstado(EstadoVenta.PENDIENTE.getCodigo());
            detalle = ventaDetalleRepository.save(detalle);
            log.info("VentaDetalle creada con ID: {}", detalle.getId());
            rabbitMQSender.enviarMensajeVenta(detalle);
        }
    }

    @Transactional
    @Override
    public boolean verificarStockDisponible(List<ProductoRequest> productos) {
        for (ProductoRequest productoRequest : productos) {
            Producto producto = productoRepository.findById(productoRequest.getIdProducto()).orElse(null);
            if (producto == null || producto.getStockDisponible() <= productoRequest.getCantidad()) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    @Override
    public boolean verificarStockDisponible(Long idProducto, int cantidad) {
        Producto producto = productoRepository.findById(idProducto).orElse(null);
        return producto != null && producto.getStockDisponible() >= cantidad;
    }

    @Transactional
    @Override
    public void actualizarStock(Long idVenta, Long idProducto, int cantidad) {
        VentaDetalle detalle = ventaDetalleRepository.findById(idVenta).orElseThrow(() -> new IllegalArgumentException("Detalle no encontrado"));
        detalle.setEstado(EstadoVenta.PROCESADO.getCodigo());
        Producto producto = productoRepository.findById(idProducto).orElseThrow(() -> new IllegalArgumentException("Producto no encontrado"));
        if (producto.getStockDisponible() >= cantidad) {
            producto.setStockDisponible(producto.getStockDisponible() - cantidad);
            producto.setFechaActualizacion(LocalDate.now());
            productoRepository.save(producto);
            ventaDetalleRepository.save(detalle);
            log.info("Stock actualizado para VentaDetalle ID: {} estado:Procesado", detalle.getId());
        } else {
            detalle.setEstado(EstadoVenta.PROCESADO_CON_ERROR.getCodigo());
            ventaDetalleRepository.save(detalle);
            log.warn("No se pudo actualizar el stock para VentaDetalle ID: {} estado:Procesado_con_error", detalle.getId());
        }

    }

    @Transactional
    @Override
    public void reprocesarVentas(Long idProducto) {
        List<VentaDetalle> detalles = ventaDetalleRepository.findByProductoIdAndEstado(idProducto, EstadoVenta.PROCESADO_CON_ERROR.getCodigo());
        for (VentaDetalle detalle : detalles) {
            log.info("Reprocesar venta para Producto ID: {} detalles con estado:Procesado_con_error", idProducto);
            actualizarStock(detalle.getId(), detalle.getProducto().getId(), detalle.getCantidad());
        }
        log.info("Sin detalles para reprocesar Producto ID: {}", idProducto);
    }
}

package py.com.prutec.stock.ventas.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import py.com.prutec.stock.ventas.model.enums.EstadoVenta;

/**
 *
 * @author pbbattilana
 */
@Data
@Entity
@Table(name = "venta_detalle")
@AllArgsConstructor
@NoArgsConstructor
public class VentaDetalle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "venta_cabecera_id", referencedColumnName = "id")
    private VentaCabecera ventaCabecera;

    @ManyToOne
    @JoinColumn(name = "producto_id", referencedColumnName = "id")
    private Producto producto;

    @Column(name = "cantidad")
    private int cantidad;
    
    @Column(name = "estado")
    private String estado;
}

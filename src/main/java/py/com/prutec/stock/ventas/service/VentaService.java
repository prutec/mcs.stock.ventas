package py.com.prutec.stock.ventas.service;

import java.util.List;
import py.com.prutec.stock.ventas.model.request.ProductoRequest;
import py.com.prutec.stock.ventas.model.request.VentaRequest;

/**
 *
 * @author pbbattilana
 */
public interface VentaService {

    public void realizarVenta(VentaRequest ventas);

    public boolean verificarStockDisponible(List<ProductoRequest> productos);

    public boolean verificarStockDisponible(Long idProducto, int cantidad);

    public void actualizarStock(Long idVenta, Long idProducto, int cantidad);

    public void reprocesarVentas(Long idProducto);
}

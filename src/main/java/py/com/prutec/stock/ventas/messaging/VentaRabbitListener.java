package py.com.prutec.stock.ventas.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.prutec.stock.ventas.model.enums.EstadoVenta;
import py.com.prutec.stock.ventas.service.impl.VentaServiceImpl;

/**
 *
 * @author pbbattilana
 */
@Slf4j
@Component
public class VentaRabbitListener {

    @Autowired
    private VentaServiceImpl ventaService;

    @RabbitListener(
            queues = "cola_venta"//, 
//            containerFactory = "comprasListenerContainerFactory"
    )
    public void realizarVentaAsync(String mensaje) {
        String[] partes = mensaje.split(":");
        Long idVenta = Long.parseLong(partes[0]);
        Long idProducto = Long.parseLong(partes[1]);
        int cantidad = Integer.parseInt(partes[2]);
        ventaService.actualizarStock(idVenta, idProducto, cantidad);
    }

    @RabbitListener(
            queues = "cola_compra_reprocesar"//, 
//            containerFactory = "comprasListenerContainerFactory"
    )
    public void reprocesarVentas(String mensaje) {
        Long idProducto = Long.parseLong(mensaje);
        ventaService.reprocesarVentas(idProducto);
    }
}

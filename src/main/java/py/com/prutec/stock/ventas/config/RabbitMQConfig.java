package py.com.prutec.stock.ventas.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author pbbattilana
 */
@Configuration
public class RabbitMQConfig {

    public static final String EXCHANGE_NAME = "exchange.stock";
    public static final String QUEUE_NAME_VENTA = "cola_venta";
    public static final String QUEUE_NAME_REPROCESAR = "cola_compra_reprocesar";

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(EXCHANGE_NAME);
    }

    @Bean
    public Queue ventaQueue() {
        return new Queue(QUEUE_NAME_VENTA);
    }

    @Bean
    public Queue reprocesarQueue() {
        return new Queue(QUEUE_NAME_REPROCESAR);
    }

    @Bean
    public Binding bindingVenta(Queue ventaQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(ventaQueue).to(directExchange).with("ventas.stock.update");
    }

    @Bean
    public Binding bindingReprocesar(Queue reprocesarQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(reprocesarQueue).to(directExchange).with("reprocesar.stock.update");
    }

//    @Bean
//    public SimpleRabbitListenerContainerFactory comprasListenerContainerFactory(ConnectionFactory connectionFactory) {
//        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
//        factory.setConnectionFactory(connectionFactory);
//        factory.setAcknowledgeMode(AcknowledgeMode.AUTO);
//        factory.setConcurrentConsumers(1);
//        factory.setDefaultRequeueRejected(false);
//        return factory;
//    }
}
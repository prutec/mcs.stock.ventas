package py.com.prutec.stock.util;

import java.time.LocalDate;
import py.com.prutec.stock.ventas.model.Cliente;
import py.com.prutec.stock.ventas.model.Producto;
import py.com.prutec.stock.ventas.model.VentaCabecera;
import py.com.prutec.stock.ventas.model.VentaDetalle;
import py.com.prutec.stock.ventas.model.enums.EstadoVenta;

public class TestUtils {

    public static Cliente createCliente() {
        Cliente cliente = new Cliente();
        cliente.setId(1l);
        cliente.setNombre("Max");
        cliente.setDireccion("Mariscal Lopez");
        cliente.setTelefono("0991123456");
        return cliente;
    }
    
    public static Producto createProducto() {
        Producto producto = new Producto();
        producto.setId(1l);
        producto.setNombre("Agua");
        producto.setCategoria("Bebidas");
        producto.setDescripcion("Agua mineral sin gas");
        producto.setFechaActualizacion(LocalDate.now());
        producto.setFechaCreacion(LocalDate.now());
        producto.setPrecio(25000);
        producto.setProveedor("aguas.a.");
        producto.setStockDisponible(15);
        return producto;
    }

    public static VentaCabecera createVentaCabecera() {
        VentaCabecera ventaCabecera = new VentaCabecera();
        ventaCabecera.setId(1l);
        ventaCabecera.setCliente(createCliente());
        ventaCabecera.setFecha(LocalDate.now());
        return ventaCabecera;
    }

    public static VentaDetalle createVentaDetalle() {
        VentaDetalle ventaDetalle = new VentaDetalle();
        ventaDetalle.setId(1l);
        ventaDetalle.setVentaCabecera(createVentaCabecera());
        ventaDetalle.setProducto(createProducto());
        ventaDetalle.setCantidad(15);
        ventaDetalle.setEstado(EstadoVenta.PENDIENTE.getCodigo());
        return ventaDetalle;
    }
}

package py.com.prutec.stock.ventas.controller;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import py.com.prutec.stock.ventas.model.Cliente;
import py.com.prutec.stock.ventas.service.impl.ClienteServiceImpl;

/**
 *
 * @author pbbattilana
 */
@ExtendWith(MockitoExtension.class)
public class ClienteControllerTest {

    @InjectMocks
    private ClienteController clienteController;
    
    @Mock
    private ClienteServiceImpl clienteService;


    @Test
    public void testCrearCliente_ClienteCreado() {
        Cliente clienteRequest = new Cliente();
        clienteRequest.setNombre("Nombre Cliente");
        clienteRequest.setDireccion("Dirección Cliente");

        Cliente clienteCreado = new Cliente();
        clienteCreado.setId(1L);
        clienteCreado.setNombre("Nombre Cliente");
        clienteCreado.setDireccion("Dirección Cliente");

        // Configurar el comportamiento del servicio para que retorne el cliente creado
        when(clienteService.crearCliente(clienteRequest)).thenReturn(clienteCreado);

        ResponseEntity<Cliente> response = clienteController.crearCliente(clienteRequest);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(clienteCreado, response.getBody());
    }
}
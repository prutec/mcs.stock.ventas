package py.com.prutec.stock.ventas.service;

import py.com.prutec.stock.ventas.service.impl.VentaServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import py.com.prutec.stock.util.TestUtils;
import py.com.prutec.stock.ventas.messaging.RabbitMQSender;
import py.com.prutec.stock.ventas.model.Cliente;
import py.com.prutec.stock.ventas.model.Producto;
import py.com.prutec.stock.ventas.model.VentaCabecera;
import py.com.prutec.stock.ventas.model.VentaDetalle;
import py.com.prutec.stock.ventas.model.enums.EstadoVenta;
import py.com.prutec.stock.ventas.model.request.ProductoRequest;
import py.com.prutec.stock.ventas.model.request.VentaRequest;
import py.com.prutec.stock.ventas.repository.ClienteRepository;
import py.com.prutec.stock.ventas.repository.ProductoRepository;
import py.com.prutec.stock.ventas.repository.VentaCabeceraRepository;
import py.com.prutec.stock.ventas.repository.VentaDetalleRepository;

@ExtendWith(MockitoExtension.class)
public class VentaServiceTest {

    @InjectMocks
    private VentaServiceImpl ventaService;

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private ProductoRepository productoRepository;

    @Mock
    private VentaCabeceraRepository ventaCabeceraRepository;

    @Mock
    private VentaDetalleRepository ventaDetalleRepository;

    @Mock
    private RabbitMQSender rabbitMQSender;

    @Test
    public void testRealizarVenta() {
        VentaRequest ventas = new VentaRequest();
        Cliente cliente = TestUtils.createCliente();
        Producto producto = TestUtils.createProducto();
        ventas.setClienteId(cliente.getId());
        List<ProductoRequest> productos = new ArrayList<>();
        ProductoRequest productoRequest = new ProductoRequest();
        productoRequest.setIdProducto(producto.getId());
        productoRequest.setCantidad(5);
        productos.add(productoRequest);
        ventas.setProductos(productos);

        when(clienteRepository.findById(ventas.getClienteId())).thenReturn(Optional.of(cliente));
        when(productoRepository.findById(productoRequest.getIdProducto())).thenReturn(Optional.of(producto));

        VentaCabecera cabecera = TestUtils.createVentaCabecera();
        when(ventaCabeceraRepository.save(any(VentaCabecera.class))).thenReturn(cabecera);
        VentaDetalle detalle = TestUtils.createVentaDetalle();
        when(ventaDetalleRepository.save(any(VentaDetalle.class))).thenReturn(detalle);

        ventaService.realizarVenta(ventas);

        verify(ventaCabeceraRepository, times(1)).save(any(VentaCabecera.class));
        verify(ventaDetalleRepository, times(1)).save(any());
        verify(rabbitMQSender, times(1)).enviarMensajeVenta(any());
    }

    @Test
    public void testVerificarStockDisponible() {
        List<ProductoRequest> productos = new ArrayList<>();
        ProductoRequest productoRequest1 = new ProductoRequest();
        productoRequest1.setIdProducto(1L);
        productoRequest1.setCantidad(5);
        productos.add(productoRequest1);

        ProductoRequest productoRequest2 = new ProductoRequest();
        productoRequest2.setIdProducto(2L);
        productoRequest2.setCantidad(3);
        productos.add(productoRequest2);

        Producto producto1 = new Producto();
        producto1.setId(1L);
        producto1.setStockDisponible(10);

        Producto producto2 = new Producto();
        producto2.setId(2L);
        producto2.setStockDisponible(5);

        when(productoRepository.findById(1L)).thenReturn(Optional.of(producto1));
        when(productoRepository.findById(2L)).thenReturn(Optional.of(producto2));

        boolean resultado = ventaService.verificarStockDisponible(productos);

        assertTrue(resultado);
    }

    @Test
    public void testVerificarStockDisponible_Long_int() {
        Long idProducto = 1L;
        int cantidadDisponible = 10;
        int cantidadSolicitada = 5;

        Producto producto = new Producto();
        producto.setId(idProducto);
        producto.setStockDisponible(cantidadDisponible);

        when(productoRepository.findById(idProducto)).thenReturn(Optional.of(producto));

        boolean resultadoSuficiente = ventaService.verificarStockDisponible(idProducto, cantidadSolicitada);
        boolean resultadoInsuficiente = ventaService.verificarStockDisponible(idProducto, cantidadDisponible + 1);

        assertTrue(resultadoSuficiente);
        assertFalse(resultadoInsuficiente);
    }

    @Test
    public void testActualizarStock() {
        Long idVenta = 1L;
        Long idProducto = 2L;
        int cantidad = 5;
        VentaDetalle detalle = new VentaDetalle();
        detalle.setId(idVenta);
        detalle.setEstado(EstadoVenta.PENDIENTE.getCodigo());
        Producto producto = new Producto();
        producto.setId(idProducto);
        producto.setStockDisponible(10);

        when(ventaDetalleRepository.findById(idVenta)).thenReturn(Optional.of(detalle));
        when(productoRepository.findById(idProducto)).thenReturn(Optional.of(producto));

        ventaService.actualizarStock(idVenta, idProducto, cantidad);

        assertEquals(EstadoVenta.PROCESADO.getCodigo(), detalle.getEstado());
        assertEquals(5, producto.getStockDisponible());
        verify(productoRepository, times(1)).save(producto);
        verify(ventaDetalleRepository, times(1)).save(detalle);
    }

//    @Test
//    public void testReprocesarVentas() {
//        Long idProducto = 1L;
//
//        List<VentaDetalle> detalles = new ArrayList<>();
//        VentaDetalle detalle1 = TestUtils.createVentaDetalle();
//        detalle1.setEstado(EstadoVenta.PROCESADO_CON_ERROR.getCodigo());
//        detalle1.setCantidad(5);
//        detalles.add(detalle1);
//
//        when(ventaDetalleRepository.findByProductoIdAndEstado(idProducto, EstadoVenta.PROCESADO_CON_ERROR.getCodigo())).thenReturn(detalles);
//        when(productoRepository.findById(anyLong())).thenReturn(Optional.of(TestUtils.createProducto()));
//
//        assertThrows(IllegalArgumentException.class, () -> ventaService.actualizarStock(null, detalle1.getProducto().getId(), detalle1.getCantidad()));
//        
//        ventaService.reprocesarVentas(idProducto);
//
//        verify(ventaDetalleRepository, times(1)).findByProductoIdAndEstado(idProducto, EstadoVenta.PROCESADO_CON_ERROR.getCodigo());
//        verify(productoRepository, times(1)).findById(idProducto);
//        verify(ventaService, times(1)).actualizarStock(1L, 1L, 5);
//    }

}

package py.com.prutec.stock.ventas.messaging;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.AmqpTemplate;
import py.com.prutec.stock.ventas.model.Producto;
import py.com.prutec.stock.ventas.model.VentaDetalle;

/**
 *
 * @author pbbattilana
 */
@ExtendWith(MockitoExtension.class)
public class RabbitMQSenderTest {

    @Mock
    private AmqpTemplate rabbitTemplate;

    @InjectMocks
    private RabbitMQSender rabbitMQSender;

    @Test
    public void testEnviarMensajeVenta() {
        VentaDetalle detalle = new VentaDetalle();
        detalle.setId(1L);

        Producto producto = new Producto();
        producto.setId(1L);
        detalle.setProducto(producto);

        detalle.setCantidad(5);

        rabbitMQSender.enviarMensajeVenta(detalle);

        // Verificar que se llame a convertAndSend con los parámetros correctos
        String expectedMensaje = "1:1:5";
        verify(rabbitTemplate).convertAndSend("exchange.stock", "ventas.stock.update", expectedMensaje);
    }
}

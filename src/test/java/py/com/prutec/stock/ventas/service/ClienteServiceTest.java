package py.com.prutec.stock.ventas.service;

import py.com.prutec.stock.ventas.service.impl.ClienteServiceImpl;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import py.com.prutec.stock.util.TestUtils;
import py.com.prutec.stock.ventas.model.Cliente;
import py.com.prutec.stock.ventas.repository.ClienteRepository;

/**
 *
 * @author pbbattilana
 */

@ExtendWith(MockitoExtension.class)
public class ClienteServiceTest {

    @InjectMocks
    private ClienteServiceImpl clienteService;
    
    @Mock
    private ClienteRepository clienteRepository;
    
    @Test
    public void testCrearCliente() {
        Cliente expResult = TestUtils.createCliente();
        when(clienteRepository.save(any(Cliente.class))).thenReturn(TestUtils.createCliente());
        Cliente result = clienteService.crearCliente(TestUtils.createCliente());
        assertEquals(expResult, result);
    }
    
}

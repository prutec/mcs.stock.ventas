package py.com.prutec.stock.ventas.messaging;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import org.mockito.junit.jupiter.MockitoExtension;
import py.com.prutec.stock.ventas.service.impl.VentaServiceImpl;

/**
 *
 * @author pbbattilana
 */
@ExtendWith(MockitoExtension.class)
public class VentaRabbitListenerTest {

    @Mock
    private VentaServiceImpl ventaService;

    @InjectMocks
    private VentaRabbitListener ventaRabbitListener;

    @Test
    public void testRealizarVentaAsync() {
        String mensaje = "1:2:5"; // mensaje simulado de la cola

        ventaRabbitListener.realizarVentaAsync(mensaje);

        // Verificar que se llama a actualizarStock con los parámetros correctos
        verify(ventaService).actualizarStock(1L, 2L, 5);
    }

    @Test
    public void testReprocesarVentas() {
        String mensaje = "1"; // mensaje simulado de la cola

        ventaRabbitListener.reprocesarVentas(mensaje);

        // Verificar que se llama a reprocesarVentas con el parámetro correcto
        verify(ventaService).reprocesarVentas(1L);
    }
}

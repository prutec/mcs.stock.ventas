package py.com.prutec.stock.ventas.controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import py.com.prutec.stock.ventas.model.request.ProductoRequest;
import py.com.prutec.stock.ventas.model.request.VentaRequest;
import py.com.prutec.stock.ventas.service.impl.VentaServiceImpl;

/**
 *
 * @author pbbattilana
 */
@ExtendWith(MockitoExtension.class)
public class VentaControllerTest {

    @Mock
    private VentaServiceImpl ventaService;

    @InjectMocks
    private VentaController ventaController;

    @Test
    public void testRealizarVenta_StockSuficiente() {
        VentaRequest ventaRequest = new VentaRequest();
        List<ProductoRequest> productos = new ArrayList<>();
        ProductoRequest productoRequest = new ProductoRequest();
        productoRequest.setIdProducto(1L);
        productoRequest.setCantidad(5);
        productos.add(productoRequest);
        ventaRequest.setProductos(productos);

        // Configurar el comportamiento del servicio para que retorne true
        when(ventaService.verificarStockDisponible(productos)).thenReturn(true);

        ResponseEntity<String> response = ventaController.realizarVenta(ventaRequest);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Compra realizada con éxito (En proceso de actualización)", response.getBody());
    }

    @Test
    public void testRealizarVenta_StockInsuficiente() {
        VentaRequest ventaRequest = new VentaRequest();
        List<ProductoRequest> productos = new ArrayList<>();
        ProductoRequest productoRequest = new ProductoRequest();
        productoRequest.setIdProducto(1L);
        productoRequest.setCantidad(5);
        productos.add(productoRequest);
        ventaRequest.setProductos(productos);

        // Configurar el comportamiento del servicio para que retorne false
        when(ventaService.verificarStockDisponible(productos)).thenReturn(false);

        ResponseEntity<String> response = ventaController.realizarVenta(ventaRequest);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Stock insuficiente para la compra", response.getBody());
    }
}
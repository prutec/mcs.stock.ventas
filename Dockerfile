# Utiliza una imagen base adecuada, como OpenJDK para aplicaciones Java
FROM openjdk:17-jdk-alpine

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia el JAR o el código fuente de tu microservicio al contenedor
COPY target/stock.ventas-0.0.1-SNAPSHOT.jar app.jar

# Expone el puerto en el que tu microservicio escucha las solicitudes
EXPOSE 9091

# Comando para ejecutar tu microservicio al iniciar el contenedor
CMD ["java", "-jar", "app.jar"]
